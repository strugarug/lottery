/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Exequiel Pierotto
 */
import java.util.ArrayList;
import java.util.List;

public class CompareNums {

    private String results;

    public void checkNums(int[][] numbers, List<Integer> raffleResults) {
        int count = 0;
        List<Integer> hits = new ArrayList<Integer>();
        StringBuilder sb = new StringBuilder();
        sb.append("These are the results:\n");
        for (int key = 0; key < numbers.length; key++) {
            count = 0;
            hits = new ArrayList<Integer>();
            for (int i = 0; i < numbers[key].length; i++) {
                for (int j = 0; j < raffleResults.size(); j++) {
                    if (raffleResults.get(j).compareTo(numbers[key][i]) == 0) {
                        //System.out.println("Hit: " + numbers[key][i]);
                        count++;
                        hits.add(raffleResults.get(j));
                    }
                }
            }
            sb.append("On line " + (key + 1) + " you have guessed " + count + " numbers:" + hits.toString() + "\n");
        }
        sb.append("The  LUCKY  numbers  were: " + raffleResults.toString() + "\n");
        results = sb.toString();
    }

    /**
     * Get the value of results
     *
     * @return the value of results
     */
    public String getResults() {
        return results;
    }
}
