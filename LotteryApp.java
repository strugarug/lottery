/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gheorghe Strugaru
 */
import java.util.ArrayList;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;

public class LotteryApp {

    /**
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {

        LuckyNos ln = new LuckyNos();
        CompareNums cn = new CompareNums();

        Scanner sc = new Scanner(System.in);

        //declaring string array to indicate in the for loop what number is the user prompted to enter
        String[] noLocations = {"first", "second", "third", "fourth", "fifth"};
        //declaring the 2d Array which will store the user numbers (3 lines of 5 numbers)
        int userNo[][] = new int[3][5];

        /*	       userNo[0][0] = 1;
		       userNo[0][1] = 2;
		       userNo[0][2] = 3;
	           userNo[0][3] = 4;
		       userNo[0][4] = 5;
		       userNo[1][0] = 6;
		       userNo[1][1] = 7;
		       userNo[1][2] = 8;
		       userNo[1][3] = 9;
		       userNo[1][4] = 10;
	           userNo[2][0] = 11;
		       userNo[2][1] = 12;
		       userNo[2][2] = 13;
		       userNo[2][3] = 14;
               userNo[2][4] = 15; */
        //declaring variables to test certain conditions --e.g numbers between 1 and 40
        int minNumber = 1, maxNumber = 41, tempNumber = 0;
        //declaring a list of integers to ensure that the 5 numbers entered on each line are unique, but they can be duplicated on different lines
        List<Integer> testList = new ArrayList<>();

        //the 2 for loops nested are asking the user for input and also testing for errors and integer duplicity on each line
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 5; j++) {
                System.out.print("Please enter " + noLocations[j] + " number on " + noLocations[i] + " line: ");
                try {
                    tempNumber = sc.nextInt();
                } catch (IndexOutOfBoundsException e) {
                    System.err.println("Index Out Of Bounds Exception: " + e.getMessage());
                } catch (InputMismatchException e) {
                    System.err.println("Number Format Exception: " + e.getMessage());
                }
                do {
                    if (tempNumber < minNumber || tempNumber > maxNumber || testList.contains(tempNumber)) {
                        //                    System.out.print("--DEBUG1"+ (tempNumber < minNumber)+ (tempNumber > maxNumber)+ (testList.contains(tempNumber))+"DEBUG -- "/*+ testList.contains(tempNumber)+ "  "*/ +tempNumber+ "  "+ testList.contains(tempNumber) +" " +userNo[i][j] + "\n");
                        System.out.print("Numbers must be unique on each line and between 1 and 40!\n");
                        System.out.print("Please RE-enter " + noLocations[j] + " number on " + noLocations[i] + " line: ");
                        try {
                            tempNumber = sc.nextInt();
                        } catch (IndexOutOfBoundsException e) {
                            System.err.println("Index Out Of Bounds Exception: " + e.getMessage());
                        } catch (NumberFormatException e) {
                            System.err.println("Number Format Exception: " + e.getMessage());
                        }
                    }
                } while (tempNumber < minNumber || tempNumber > maxNumber || testList.contains(tempNumber));

                userNo[i][j] = tempNumber;
                testList.add(tempNumber);
            }
            testList.clear();
        }

        ln.Add();
        cn.checkNums(userNo, ln.getLucky());
        System.out.println(cn.getResults());
    }

}
